<!DOCTYPE html>
<html>
<head>

		<style>

			body {
 
  background: linear-gradient(45deg, #49a09d, #5f2c82);
}
.sep{
	margin-left: 30%;
}
.elim{
	color:yellow;
}

.comp {
	box-shadow:inset 0px 1px 0px 0px #a4e271;
	background:linear-gradient(to bottom, #89c403 5%, #77a809 100%);
	background-color:#89c403;
	border-radius:6px;
	border:1px solid #74b807;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
	font-size:15px;
	font-weight:bold;
	padding:6px 24px;
	text-decoration:none;
	text-shadow:0px 1px 0px #528009;

}
.comp:hover {
	background:linear-gradient(to bottom, #77a809 5%, #89c403 100%);
	background-color:#77a809;
}
.comp:active {
	position:relative;
	top:1px;
}
	</style>

	<script>

function funcionAsociadaBoton(){

  window.location.href="entrar1.php";
}

  </script>
</head>
<body>

<?php
session_start();

if (isset($_POST['entrar'])){

	$_SESSION['usuario'] = $_POST['usuario'];
}

 $servername = "localhost";
    $username = "administrador";
    $password = "administrador";
    $dbname = "ventas";

$connect = mysqli_connect($servername, $username, $password,$dbname);


if(isset($_POST["add_to_cart"]))
{
if(isset($_SESSION["shopping_cart"]))
{
$item_array_id = array_column($_SESSION["shopping_cart"], "item_id");
if(!in_array($_GET["id"], $item_array_id))
{
$count = count($_SESSION["shopping_cart"]);
$item_array = array(
'item_id' => $_GET["id"],
'item_name' => $_POST["hidden_name"],
'item_price' => $_POST["hidden_price"],
'item_quantity' => $_POST["quantity"]
);
$_SESSION["shopping_cart"][$count] = $item_array;
}
else
{
echo '<script>alert("El producto ya se encuentra agregado")</script>';
 
}
}
else
{
$item_array = array(
'item_id' => $_GET["id"],
'item_name' => $_POST["hidden_name"],
'item_price' => $_POST["hidden_price"],
'item_quantity' => $_POST["quantity"]
);
$_SESSION["shopping_cart"][0] = $item_array;
}
}
if(isset($_GET["action"]))
{
if($_GET["action"] == "delete")
{
foreach($_SESSION["shopping_cart"] as $keys => $values)
{
if($values["item_id"] == $_GET["id"])
{
unset($_SESSION["shopping_cart"][$keys]);
}
}
}
}
?>


  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" /> 


<body>
<div class="container" style="width:800px;">

<h2> <font color="FFFFFF">Hola <?php echo ($_SESSION['usuario']);?>, bienvenido/a a nuestra tienda online</h2>
<?php
$query = "SELECT * FROM articulos ORDER BY id ASC";
$result = mysqli_query($connect, $query);
if(mysqli_num_rows($result) > 0)
{
while($row = mysqli_fetch_array($result))
{
?>
<div class="col-md-4">
<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>?action=add&id=<?php echo $row["id"]; ?>">
<div class="thumbnail">
<img src="<?php echo $row["imagen"]; ?>" class="img-responsive" />
<div class="caption">
<h4 class="text-info text-center"><?php echo $row["nombre"]; ?></h4>
<h4 class="text-danger text-center">$ <?php echo $row["precio"]; ?></h4>
<input type="text" name="quantity" class="form-control" value="1" />
<p class='text-center'><br>
<input type="submit" name="add_to_cart" class="btn btn-success " value="Agregar al carro" /></p>
 
<input type="hidden" name="hidden_name" value="<?php echo $row["nombre"]; ?>" />
<input type="hidden" name="hidden_price" value="<?php echo $row["precio"]; ?>" />
</div>
</div>
</form>
</div>
<?php
}

}
?>
<form action = "confirmar.php" method="post">
<div style="clear:both"></div>

<h3>Carrito de compra</h3>
<div class="table-responsive">
<table class="table table-bordered">

<tr>
<th width="40%">Descripción</th>
<th width="10%" class='text-center'>Cantidad</th>
<th width="20%" class='text-right'>Precio</th>
<th width="15%" class='text-right'>Total</th>
<th width="5%"></th>
</tr>
<br>
<br><br>

<?php

$f= date("Y").'-'.date("m").'-'.date("d").' '.date("h").':'.date("i").':'.date("s");
echo $f."<br>";

if(!empty($_SESSION["shopping_cart"]))
{
$total = 0;
foreach($_SESSION["shopping_cart"] as $keys => $values)
{
?>
<tr>
<td><?php echo $values["item_name"]; ?></td>
<td class='text-center'><?php echo $values["item_quantity"]; ?></td>
<td class='text-right'>$ <?php echo $values["item_price"]; ?></td>
<td class='text-right'>$ <?php echo number_format($values["item_quantity"] * $values["item_price"], 2); ?></td>
<td><a href="index.php?action=delete&id=<?php echo $values["item_id"]; ?>"><span class="elim">Eliminar</span></a></td>
</tr>
<?php
$total = $total + ($values["item_quantity"] * $values["item_price"]);

}

?>
<tr>
<td colspan="3" align="right">Total</td>
<td align="right">$ <?php echo number_format($total, 2); ?></td>
<button class="comp" type="submit" name="comprar" value="Comprar">comprar</button>
<td></td>
</tr>
<?php
}
?>


</table>
</div>
</div>
</button>
<div class="sep">
<form action = "confirmar.php" method="post">
<input class="comp" type="button" value="Volver atras" onClick="funcionAsociadaBoton()">
<br><br><br>
</form>

</div>


</body>
</html> 