
<html>
<head>

<script>

function funcionAsociadaBoton(){

  window.location.href="entrar1.php";
}

  </script>

  <style>

html { box-sizing: border-box; }

*, *:before, *:after {
  -moz-box-sizing: border-box;
  -webkit-box-sizing: border-box;
  box-sizing: border-box;
}

body {
  font-family: 'Nunito',sans-serif;
  color: #384047;
  background: linear-gradient(45deg, #49a09d, #5f2c82);
}

form {
  max-width: 300px;
  margin: 10px auto;
  padding: 10px 20px;
  background: #f4f7f8;
  border-radius: 8px;
  border: 1px solid #8265B0;
  box-shadow: 3px 3px 3px rgba(0,0,0,0.2)
}

h1 {
  margin: 0 0 30px 0;
  text-align: center;
}

input[type="text"],
input[type="password"],
input[type="date"],
input[type="datetime"],
input[type="email"],
input[type="number"],
input[type="search"],
input[type="tel"],
input[type="time"],
input[type="url"],
textarea,
select {
  background: rgba(255,255,255,0.1);
  border: none;
  font-size: 16px;
  height: auto;
  margin: 0;
  outline: 0;
  padding: 15px;
  width: 100%;
  background-color: #e8eeef;
  color: #8a97a0;
  box-shadow: 0 1px 0 rgba(0,0,0,0.03) inset;
  margin-bottom: 30px;
}


input[type="radio"],
input[type="checkbox"] {
  margin: 0 4px 8px 0;
}

select {
  padding: 6px;
  height: 32px;
  border-radius: 2px;
}

button {
  padding: 19px 39px 18px 39px;
  color: #FFF;
  background-color: #A085C6;
  /*#4bc970*/
  font-size: 18px;
  text-align: center;
  font-style: normal;
  border-radius: 5px;
  width: 100%;
  border: 1px solid #8265B0;
  /*#3ac162*/
  border-width: 1px 1px 3px;
  box-shadow: 0 -1px 0 rgba(255,255,255,0.1) inset;
  margin-bottom: 10px;
}

fieldset {
  margin-bottom: 30px;
  border: none;
}

legend {
  font-size: 1.4em;
  margin-bottom: 10px;
}

label {
  display: block;
  margin-bottom: 8px;
}

label.light {
  font-weight: 300;
  display: inline;
}

.number {
  background-color: #A085C6;
  /*#5fcf80*/
  color: #fff;
  height: 30px;
  width: 30px;
  display: inline-block;
  font-size: 0.8em;
  margin-right: 4px;
  line-height: 30px;
  text-align: center;
  text-shadow: 0 1px 0 rgba(255,255,255,0.2);
  border-radius: 100%;
}

abbr[title] {
  border-bottom-width: 0;
}


@media screen and (min-width: 480px) {

  form {
    max-width: 480px;
  }

}



.comp {
  box-shadow:inset 0px 1px 0px 0px #a4e271;
  background:linear-gradient(to bottom, #89c403 5%, #77a809 100%);
  background-color:#89c403;
  border-radius:6px;
  border:1px solid #74b807;
  display:inline-block;
  cursor:pointer;
  color:#ffffff;
  font-family:Arial;
  font-size:15px;
  font-weight:bold;
  padding:6px 24px;
  text-decoration:none;
  text-shadow:0px 1px 0px #528009;

}
.comp:hover {
  background:linear-gradient(to bottom, #77a809 5%, #89c403 100%);
  background-color:#77a809;
}
.comp:active {
  position:relative;
  top:1px;
}

.salir {
  box-shadow: 3px 4px 0px 0px #8a2a21;
  background:linear-gradient(to bottom, #c62d1f 5%, #f24437 100%);
  background-color:#c62d1f;
  border-radius:18px;
  border:1px solid #d02718;
  display:inline-block;
  cursor:pointer;
  color:#ffffff;
  font-family:Arial;
  font-size:13px;
  padding:7px 25px;
  text-decoration:none;
  text-shadow:0px 1px 0px #810e05;
  margin-left: 40%;
}
.salir:hover {
  background:linear-gradient(to bottom, #f24437 5%, #c62d1f 100%);
  background-color:#f24437;
}
.salir:active {
  position:relative;
  top:1px;
}


.sep{
  margin-left: 18%;

}


  </style>
</head>

<body>



<?php
  session_start();
  if(isset($_SESSION["usuario"]) && isset($_SESSION["rol"])) {

  if ($_SESSION["rol"]=="administrador"){
    $servername = "localhost";
    $username = "administrador";
    $password = "administrador";
    $dbname = "ventas";


    $conn = mysqli_connect($servername, $username, $password,$dbname);
    if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
     }
  }else{
    
    $servername = "localhost";
    $username = "consultor";
    $password = "consultor";
    $dbname = "ventas";


    $conn = mysqli_connect($servername, $username, $password,$dbname);
  

    if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
    }
  }

?>



  <br>
<h2 align="center"><font color="000000">Hola <?php echo ($_SESSION['usuario']);?> </h2>
<h3 align="center">Aqui puedes añadir nuevos articulos a la base de datos</h3>


<form  action="" method="post">


Nombre <input name="nom" type="text"><br>
Imagen <input name="imag" type="text"><br>
Precio <input name="precio" type="text"><br>
<div class="sep">
<input class="comp" type="submit" value="Añadir articulos" name="anadir">
<input class="comp" type="button" value="Volver atras" onClick="funcionAsociadaBoton()">
</div><br><br>
<input class="salir" type="submit" value="Cerrar Sesion"  name="salir">
</form>
<?php } ?>


<?php

if (isset($_POST['anadir'])){

  
    $nom=$_POST["nom"];
    $imag=$_POST["imag"];
    $prec=$_POST["precio"];
    
    $sql1 = "INSERT INTO articulos (id, nombre, imagen, precio)
    VALUES (NULL, '$nom', '$imag', '$prec')";


      if (mysqli_query($conn, $sql1)) {
        echo "<br><p align='center'> Se ha creado con exito</p>";
      }else{
        echo "<br><p> Error Privilegios insuficientes </p>";
      }


 
}
 if (isset($_POST['salir'])){

  session_destroy();
   

  header("location:form.php");
  }
?>


</body>
</html>