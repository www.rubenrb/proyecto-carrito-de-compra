
<html>
<head>
  
<script>

function funcionAsociadaBoton(){

  window.location.href="entrar1.php";
}

  </script>

  <style>


      table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
  text-align: center;
}

html,
body {
  height: 100%;
}

body {
  margin: 0;
  background: linear-gradient(45deg, #49a09d, #5f2c82);
  font-family: sans-serif;
  font-weight: 100;
}
.sep{
  margin-left: 20%;
  margin-top:30%;
}
.container {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}

table {

  width: 1200px;
  border-collapse: collapse;
  overflow: hidden;
  box-shadow: 0 0 20px rgba(0,0,0,0.1);
}

th,
td {
  padding: 15px;
  background-color: rgba(255,255,255,0.2);
  color: #fff;
}

th {
  text-align: center;
}

thead {
  th {
    background-color: #55608f;
  }
}

tbody {
  tr {
    &:hover {
      background-color: rgba(255,255,255,0.3);
    }
  }
  td {
    position: relative;
    &:hover {
      &:before {
        content: "";
        position: absolute;
        left: 0;
        right: 0;
        top: -9999px;
        bottom: -9999px;
        background-color: rgba(255,255,255,0.2);
        z-index: -1;
      }
    }
  }
}
.comp {
  box-shadow:inset 0px 1px 0px 0px #a4e271;
  background:linear-gradient(to bottom, #89c403 5%, #77a809 100%);
  background-color:#89c403;
  border-radius:6px;
  border:1px solid #74b807;
  display:inline-block;
  cursor:pointer;
  color:#ffffff;
  font-family:Arial;
  font-size:15px;
  font-weight:bold;
  padding:6px 24px;
  text-decoration:none;
  text-shadow:0px 1px 0px #528009;
  
  

}
.comp:hover {
  background:linear-gradient(to bottom, #77a809 5%, #89c403 100%);
  background-color:#77a809;
}
.comp:active {
  position:relative;
  top:1px;
}

.salir {
  box-shadow: 3px 4px 0px 0px #8a2a21;
  background:linear-gradient(to bottom, #c62d1f 5%, #f24437 100%);
  background-color:#c62d1f;
  border-radius:18px;
  border:1px solid #d02718;
  display:inline-block;
  cursor:pointer;
  color:#ffffff;
  font-family:Arial;
  font-size:13px;
  padding:7px 25px;
  text-decoration:none;
  text-shadow:0px 1px 0px #810e05;
 
}
.salir:hover {
  background:linear-gradient(to bottom, #f24437 5%, #c62d1f 100%);
  background-color:#f24437;
}
.salir:active {
  position:relative;
  top:1px;
}
  </style>
</head>

<body>



<?php
  session_start();
  if(isset($_SESSION["usuario"])) {

  
  
  

?>

<br>
<h2 align="center">Hola <?php echo ($_SESSION['usuario']);?> </h2>

<h3 align="center">Aqui puedes ver una lista de todos nuestros productos</h3>

<?php } ?>


<?php


  
    $servername = "localhost";
    $username = "consultor";
    $password = "consultor";
    $dbname = "ventas";

$conn = mysqli_connect($servername, $username, $password,$dbname);

if (!$conn) {
 die("Connection failed: " . mysqli_connect_error());
}






  $sql = " SELECT * FROM articulos";

  $result = mysqli_query ($conn, $sql);


  if ($result == FALSE) {
echo "Error en la ejecución de la consulta.<br />";

}
 else {

 echo "<div class='container'>";
    echo "<table>";
echo "<tr>";
echo "<th>" ."ID Del articulo "."</th>";
echo "<th>" ."Descripcion "."</th>";
echo "<th>" ." Precio "."</th>";
echo "</tr>";


while ($registro = mysqli_fetch_row($result)) {
echo "<tr>";
echo "<td>" .$registro[0]."</td>";
echo "<td>" .$registro[1]."</td>";
echo "<td>" .$registro[3]."€"."</td>";

echo "</tr>";


}
echo "</table>"."<br>";
echo "</div>"."<br>";
}
?>

<form  action="" method="post">
  <div class="sep">
<input class="comp" class="sep" type="button" value="Volver atras" onClick="funcionAsociadaBoton()">
<input  class="salir" type="submit" value="Cerrar Sesion"  name="salir">
</div>
</form>

</div>
<?php
  if (isset($_POST['salir'])){


    session_destroy();
     

    header("location:form.php");
  }

  mysqli_close($conn);
?>

</body>
</html>