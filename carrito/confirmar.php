<!DOCTYPE html>
<html>
<head>



  <style>
body {

  background: linear-gradient(90deg, #49a09d, #5f2c82);
}

.salir {
  box-shadow: 3px 4px 0px 0px #8a2a21;
  background:linear-gradient(to bottom, #c62d1f 5%, #f24437 100%);
  background-color:#c62d1f;
  border-radius:18px;
  border:1px solid #d02718;
  display:inline-block;
  cursor:pointer;
  color:#ffffff;
  font-family:Arial;
  font-size:13px;
  padding:7px 25px;
  text-decoration:none;
  text-shadow:0px 1px 0px #810e05;
 
}
.salir:hover {
  background:linear-gradient(to bottom, #f24437 5%, #c62d1f 100%);
  background-color:#f24437;
}
.salir:active {
  position:relative;
  top:1px;
}
  </style>

</head>
<body>
<?php
	session_start();
	if (isset($_SESSION['usuario']) && isset($_POST['comprar'])){

	$servername = "localhost";
    $username = "administrador";
    $password = "administrador";
    $dbname = "ventas";

$connect = mysqli_connect($servername, $username, $password,$dbname);
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" /> 
<div style="clear:both"></div>
<h3><font color="FFFFFF">Detalle de la orden</h3>
<div class="table-responsive">
<table class="table table-bordered">
<tr>
<th width="40%">Descripción</th>
<th width="10%" class='text-center'>Cantidad</th>
<th width="20%" class='text-right'>Precio</th>
<th width="15%" class='text-right'>Total</th>

</tr>
<br>
<br><br>

<?php

$f= date("Y").'-'.date("m").'-'.date("d").' '.date("h").':'.date("i").':'.date("s");
echo " ". $f."<br>";

if(!empty($_SESSION["shopping_cart"]))
{
$total = 0;
foreach($_SESSION["shopping_cart"] as $keys => $values)
{
?>
<tr>
<td><?php echo $values["item_name"]; ?></td>
<td class='text-center'><?php echo $values["item_quantity"]; ?></td>
<td class='text-right'>$ <?php echo $values["item_price"]; ?></td>
<td class='text-right'>$ <?php echo number_format($values["item_quantity"] * $values["item_price"], 2); ?></td>

</tr>
<?php
$total = $total + ($values["item_quantity"] * $values["item_price"]);


$user=$_SESSION['usuario'];

$precio=$values["item_price"];
$artID=$values["item_id"];
$cantidad=$values["item_quantity"];
//$idUsu=$_SESSION['usuario'];

$consulta1 = "SELECT ID_Usuarios FROM usuarios WHERE Usuario = '$user'";
$result = mysqli_query ($connect, $consulta1);

$idUsu="";

if(mysqli_num_rows($result) > 0){ 
$registro = mysqli_fetch_row($result);
 $idUsu=$registro[0];
	}



$sql1 = "INSERT INTO compras (idUsua,idArti,fecha,cantidad,precioUni) VALUES ('$idUsu','$artID','$f','$cantidad','$precio')";
if (!mysqli_query($connect, $sql1)) {
	 			echo " <br> Error: " . $sql1 . "<br>" . mysqli_error($connect);
			}

}



?>
<tr>
<td colspan="3" align="right">Total</td>
<td align="right">$ <?php echo number_format($total, 2); ?></td>


</tr>
<?php
}
?>


</table>
</div>
</div>

<?php } 




?>
<button class="salir" type="submit" name="salir" value="Salir" onclick="location.href='form.php';">Cerrar sesion</button>


<?php
session_destroy();

?>


</body>
</html> 