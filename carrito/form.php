<!DOCTYPE html>
<html>
<head>
<style>

	*{
  margin:0;
  padding:0;
  -webkit-box-sizing:border-box;
  -moz-box-sizing:border-box;
  box-sizing:border-box;
}

 #rojo{
    color:#EE280C;
  }

body{
  font-family: 'Open Sans', sans-serif;
  background: linear-gradient(99deg, #49a09d, #5f2c82);
}

svg{
  position:fixed;
  top:10px;
  left:180px;
}

.container{
  position:relative;
  top:200px;
  left:35%;
  display:block;
  margin-bottom:80px;
  width:500px;
  height:360px;
  background:#fff;
  border-radius:5px;
  overflow:hidden;
  z-index:1;
}

h2{
  padding:40px;
  font-weight:lighter;
  text-transform:uppercase;
  color:#414141;
}

input{
  display:block;
  height:50px;
  width:90%;
  margin:0 auto;
  border:none;
  &::placeholder{
    -webkit-transform:translateY(0px);
      transform:translateY(0px);
    -webkit-transition:.5s;
      transition:.5s;
  }
  &:hover,
  &:focus,
  &:active:focus{
    color:#ff5722;
    outline:none;
    border-bottom:1px solid #ff5722;
    &::placeholder{
      color:#ff5722;
      position:relative;
      -webkit-transform:translateY(-20px);
      transform:translateY(-20px);
      
    }
  }
}



.email,
.pwd{
  position:relative;
  z-index:1;
  border-bottom:1px solid rgba(0,0,0,.1);
  padding-left:20px;
  font-family: 'Open Sans', sans-serif;
  color:#858585;
  font-weight:lighter;
  -webkit-transition:.5s;
  transition:.5s;
}

.register{

	color: black;
	background-color: #46C5E1;
}
.signin{

	color: black;
	background-color: #5CE15C;
}

.link{
  text-decoration:none;
  display:inline-block;
  margin:27px 28%;
  text-transform:uppercase;
  color:#858585;
  font-weight:lighter;
  -webkit-transition:.5s;
  transition:.5s;
}



button{
  cursor:pointer; 
  display:inline-block;
  float:left;
  width:250px;
  height:60px;
  margin-top:-10px;
  border:none;
  font-family: 'Open Sans', sans-serif;
  text-transform:uppercase;
  color:#fff;
  -webkit-transition:.5s;
  transition:.5s;
  &:nth-of-type(1){
    background:#673ab7;
  }
  &:nth-of-type(2){
    background:#ff5722;
  }
  span{
    position:absolute;
    display:block;
    margin:-10px 20%;
    -webkit-transform:translateX(0);
    transform:translateX(0);
    -webkit-transition:.5s;
    transition:.5s;
  }
  &:hover{
    span{
      -webkit-transform:translateX(30px);
      transform:translateX(30px);
      }
   }
}



h3{
  position:absolute;
  top:-100%;
  left:20%; 
  text-transform:uppercase;
  font-weight:bolder;
  color:rgba(255,255,255,.3);
  -webkit-transition:.3s ease-in-out 1.2s;
  transition:.3s ease-in-out 1.2s;
}




</style>

</head>

<body>
<?php

$nombreError= $passError  = "";
$usuario = $password=  "";

if ($_SERVER["REQUEST_METHOD"] == "POST") { 
  if (empty($_POST["usuario"])) {
      $nombreError = "El campo Nombre es requerido";
  }else{
      $usuario = test_input($_POST["usuario"]);

        if (!preg_match("/^[a-zA-Z ]*$/",$usuario)) { 
          $nombreError = "Solo se permiten letras y espacios en blanco";
        }
  }

  


  if (empty($_POST["password"])) {
      $passError = "El campo Contraseña es requerido";
  }else {
      $password = test_input($_POST["password"]);

  }

  

}

function test_input($data) {
      $data = trim($data); 
      $data = stripslashes($data); 
     return $data;
}



  ?>



<div class="container">
  <h2>login</h2>
  <form action = "" method="post">
    <input type="text" class="email" name="usuario" placeholder="Usuario"><span id="rojo">* <?php echo $nombreError;?></span>
    <br/>
    <input type="text" class="pwd"  name="password" placeholder="Contraseña"><span id="rojo">* <?php echo $passError;?></span>
  

  <br><br><br>

    <button class="signin" type="submit" name="ingresar" value="Entrar">
    <span>Entrar</span>
  </button>

  </form>
  <button class="register" onclick="location.href='registro.php';">
    <span>Registrarse</span>
  </button>


 
 
</div>


<?php



  $servername = "localhost";
  $username = "consultor";
  $password = "consultor";
  $dbname = "ventas";


  $conn = mysqli_connect($servername, $username, $password,$dbname);

  if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
  }

  if(!empty($usuario) && !empty($password)){
    if (isset($_POST['ingresar'])){

        $usuario=$_POST["usuario"];
        $pass=$_POST["password"];
        $encript= hash_hmac('sha512', '$pass', 'secret');

        $administrador = "SELECT Usuario,Rol FROM usuarios WHERE Usuario = '$usuario' AND Password = '$encript' AND rol='administrador'";
        $consultor = "SELECT Usuario,Rol FROM usuarios WHERE Usuario = '$usuario' AND Password = '$encript' AND rol='consultor'";
        
        $result1 = mysqli_query ($conn, $administrador);
        $result2 = mysqli_query ($conn, $consultor);
   

    $_SESSION['usuario']="";
    $_SESSION['rol']="";


       if(mysqli_num_rows($result1) > 0){ 
      
        session_start();

        $registro = mysqli_fetch_row($result1);
        $_SESSION['rol']=$registro[1];
        
      
        $_SESSION['usuario']="$usuario";
        
        
     
          header("Location: entrar1.php");
   
          exit(); 

        }if(mysqli_num_rows($result2) > 0){  
      
          session_start();
     
          $_SESSION['usuario']="$usuario";
          $_SESSION['rol']="consultor";
     
         header("Location: entrar1.php");
   
          exit(); 

       

        }else{
  			echo "<br><br><br><br><br><br><br><br><br><br>";
           $mensajeaccesoincorrecto = "El usuario y la contraseña son incorrectos o el usuario no existe, por favor vuelva a introducirlos.";
           echo "<p align='center'>". $mensajeaccesoincorrecto . "</p>";
        }
    }
  }
  mysqli_close($conn);
?>

<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="800px" height="600px" viewBox="0 0 800 600" enable-background="new 0 0 800 600" xml:space="preserve">
<linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="174.7899" y1="186.34" x2="330.1259" y2="186.34" gradientTransform="matrix(0.8538 0.5206 -0.5206 0.8538 147.9521 -79.1468)">
	<stop  offset="0" style="stop-color:#FFC035"/>
	<stop  offset="0.221" style="stop-color:#F9A639"/>
	<stop  offset="1" style="stop-color:#E64F48"/>
</linearGradient>
<circle fill="url(#SVGID_1_)" cx="266.498" cy="211.378" r="77.668"/>
<linearGradient id="SVGID_2_" gradientUnits="userSpaceOnUse" x1="290.551" y1="282.9592" x2="485.449" y2="282.9592">
	<stop  offset="0" style="stop-color:#FFA33A"/>
	<stop  offset="0.0992" style="stop-color:#E4A544"/>
	<stop  offset="0.9624" style="stop-color:#00B59C"/>
</linearGradient>
<circle fill="url(#SVGID_2_)" cx="388" cy="282.959" r="97.449"/>
<linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="180.3469" y1="362.2723" x2="249.7487" y2="362.2723">
	<stop  offset="0" style="stop-color:#12B3D6"/>
	<stop  offset="1" style="stop-color:#7853A8"/>
</linearGradient>
<circle fill="url(#SVGID_3_)" cx="215.048" cy="362.272" r="34.701"/>
<linearGradient id="SVGID_4_" gradientUnits="userSpaceOnUse" x1="367.3469" y1="375.3673" x2="596.9388" y2="375.3673">
	<stop  offset="0" style="stop-color:#12B3D6"/>
	<stop  offset="1" style="stop-color:#7853A8"/>
</linearGradient>
<circle fill="url(#SVGID_4_)" cx="482.143" cy="375.367" r="114.796"/>
<linearGradient id="SVGID_5_" gradientUnits="userSpaceOnUse" x1="365.4405" y1="172.8044" x2="492.4478" y2="172.8044" gradientTransform="matrix(0.8954 0.4453 -0.4453 0.8954 127.9825 -160.7537)">
	<stop  offset="0" style="stop-color:#FFA33A"/>
	<stop  offset="1" style="stop-color:#DF3D8E"/>
</linearGradient>
<circle fill="url(#SVGID_5_)" cx="435.095" cy="184.986" r="63.504"/>
</svg>


</body>
</html>
