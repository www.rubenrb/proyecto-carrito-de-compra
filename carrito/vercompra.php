
<html>
<head>
  <style>


#rojo{
  color:#EE280C;
}

      table, th, td {
  border: 1px solid black;
  border-collapse: collapse;
  text-align: center;
}

html,
body {
  height: 100%;
}

body {
  margin: 0;
  background: linear-gradient(99deg, #49a09d, #5f2c82);
  font-family: sans-serif;
  font-weight: 100;
}
.sep{
  margin-left: 20%;
  margin-top:30%;
}

table {

  width: 1200px;
  border-collapse: collapse;
  overflow: hidden;
  box-shadow: 0 0 20px rgba(0,0,0,0.1);
  margin-left:15%;
}
.separar{
  margin-left:10px;
}

th{
  color:yellow;
   padding: 15px;
  background-color: rgba(255,255,255,0.2);
}
td {
  padding: 15px;
  background-color: rgba(255,255,255,0.2);
  color: #fff;
}

th {
  text-align: center;
}

thead {
  th {
    background-color: #55608f;
  }
}

tbody {
  tr {
    &:hover {
      background-color: rgba(255,255,255,0.3);
    }
  }
  td {
    position: relative;
    &:hover {
      &:before {
        content: "";
        position: absolute;
        left: 0;
        right: 0;
        top: -9999px;
        bottom: -9999px;
        background-color: rgba(255,255,255,0.2);
        z-index: -1;
      }
    }
  }
}
.comp {
  box-shadow:inset 0px 1px 0px 0px #a4e271;
  background:linear-gradient(to bottom, #89c403 5%, #77a809 100%);
  background-color:#89c403;
  border-radius:6px;
  border:1px solid #74b807;
  display:inline-block;
  cursor:pointer;
  color:#ffffff;
  font-family:Arial;
  font-size:15px;
  font-weight:bold;
  padding:6px 24px;
  text-decoration:none;
  text-shadow:0px 1px 0px #528009;
  
  

}
.comp:hover {
  background:linear-gradient(to bottom, #77a809 5%, #89c403 100%);
  background-color:#77a809;
}
.comp:active {
  position:relative;
  top:1px;
}


.salir {
  box-shadow: 3px 4px 0px 0px #8a2a21;
  background:linear-gradient(to bottom, #c62d1f 5%, #f24437 100%);
  background-color:#c62d1f;
  border-radius:18px;
  border:1px solid #d02718;
  display:inline-block;
  cursor:pointer;
  color:#ffffff;
  font-family:Arial;
  font-size:13px;
  padding:7px 25px;
  text-decoration:none;
  text-shadow:0px 1px 0px #810e05;
 
}
.salir:hover {
  background:linear-gradient(to bottom, #f24437 5%, #c62d1f 100%);
  background-color:#f24437;
}
.salir:active {
  position:relative;
  top:1px;
}

  </style>
</style>
<script>

function funcionAsociadaBoton(){

  window.location.href="entrar1.php";
}

  </script>
</head>

<body>



<?php
  session_start();
  if(isset($_SESSION["usuario"])) {

    
  
$fechaErr= "";
$fecha1  =$fecha2= "";
$fecha1  =$fecha2= false;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
if((!empty($_POST["fecha1"])) && (!empty($_POST["fecha2"]))) {
      
         $fecha1 = true;
         $fecha2 = true;
      }

      if((empty($_POST["fecha1"])) && (!empty($_POST["fecha2"]))) {
        $fechaErr = "No se puede introducir solo una fecha";
      }

      if((!empty($_POST["fecha1"])) && (empty($_POST["fecha2"]))) {
        $fechaErr = "No se puede introducir solo una fecha";
      }
  
}
?>



  
<h2 align="center">Hola <?php echo ($_SESSION['usuario']);?> </h2>

<h3 align="center">Aqui puedes consultar todas tus compras</h3>
<br><br>
<div class="separar">
<form action="<?php ($_SERVER["PHP_SELF"]);?>" method="post">
<label for="start">Fecha inicial:</label>
<input type="date" id="start" name="fecha1"
       value="0000-00-00"
       min="1900-01-01" max="2020-12-31">
        <br><br>

<label for="start">Fecha final:</label>
<input type="date" id="start" name="fecha2"
       value="0000-00-00"
       min="1900-01-01" max="2020-12-31">
       <br>
<span id="rojo"><br> <?php echo $fechaErr;?></span><br><br>
</div>

<input class="comp" type="submit" value="Ver compras"  name="ver"><br><br><br>
</form>

<?php } ?>


<?php


  
    $servername = "localhost";
    $username = "consultor";
    $password = "consultor";
    $dbname = "ventas";

$conn = mysqli_connect($servername, $username, $password,$dbname);

if (!$conn) {
 die("Connection failed: " . mysqli_connect_error());
}


if(($fecha1==true)&&($fecha2==true)){
  if (isset($_POST['ver'])){


    $fecha1=$_POST['fecha1'];
    $fecha2=$_POST['fecha2'];

    $f1=$fecha1.' '.'00'.':'.'00'.':'.'00';
    $f2=$fecha2.' '.'23'.':'.'59'.':'.'59';

$usuas=$_SESSION['usuario'];

    

$sql = " SELECT usuarios.Usuario, articulos.nombre, compras.fecha, compras.cantidad, compras.precioUni FROM compras INNER JOIN usuarios ON compras.idUsua= usuarios.ID_Usuarios INNER JOIN articulos ON compras.idArti=articulos.id WHERE (fecha >= '$f1' AND fecha <= '$f2') AND ( usuarios.Usuario='$usuas')";

  //$sql = " SELECT * FROM compras WHERE fecha >= '$f1' and fecha <= '$f2' ";

  $result = mysqli_query ($conn, $sql);


    if ($result == FALSE) {
        echo "Error en la ejecución de la consulta.<br />";

    }else {
        echo "<div class='container'>";
        echo "<table style='width:70%'>";
        echo "<tr>";
        echo "<th>" ."Usuario "."</th>";
        echo "<th>" ."Articulo "."</th>";
        echo "<th>" ." Fecha de compra "."</th>";
        echo "<th>" ." Cantidad "."</th>";
        echo "<th>" ." Precio unidad "."</th>";
        echo "</tr>";


      while ($registro = mysqli_fetch_row($result)) {
            echo "<tr>";
            echo "<td>" .$registro[0]."</td>";
            echo "<td>" .$registro[1]."</td>";
            echo "<td>" .$registro[2]."</td>";
            echo "<td>" .$registro[3]."</td>";
            echo "<td>" .$registro[4]."€"."</td>";

            echo "</tr>";


      }
      echo "</table>"."<br>";
      echo "</div>";
    }

  }
}

if((empty($_POST["fecha1"])) && (empty($_POST["fecha2"]))) {

  if (isset($_POST['ver'])){


    

        $usuas=$_SESSION['usuario'];

    

$sql = " SELECT usuarios.Usuario, articulos.nombre, compras.fecha, compras.cantidad, compras.precioUni FROM compras INNER JOIN usuarios ON compras.idUsua= usuarios.ID_Usuarios INNER JOIN articulos ON compras.idArti=articulos.id WHERE ( usuarios.Usuario='$usuas')";

  //$sql = " SELECT * FROM compras WHERE fecha >= '$f1' and fecha <= '$f2' ";

  $result = mysqli_query ($conn, $sql);


    if ($result == FALSE) {
        echo "Error en la ejecución de la consulta.<br />";

    }else {

        echo "<table style='width:70%'>";
        echo "<tr>";
        echo "<th>" ."Usuario "."</th>";
        echo "<th>" ."Articulo "."</th>";
        echo "<th>" ." Fecha de compra "."</th>";
        echo "<th>" ." Cantidad "."</th>";
        echo "<th>" ." Precio unidad "."</th>";
        echo "</tr>";


      while ($registro = mysqli_fetch_row($result)) {
            echo "<tr>";
            echo "<td>" .$registro[0]."</td>";
            echo "<td>" .$registro[1]."</td>";
            echo "<td>" .$registro[2]."</td>";
            echo "<td>" .$registro[3]."</td>";
            echo "<td>" .$registro[4]."€"."</td>";

            echo "</tr>";


      }
      echo "</table>"."<br>";
    }

  }
}








?>

<form  action="" method="post">

  <input class="comp" type="button" value="Volver atras" onClick="funcionAsociadaBoton()">
<input class="salir" type="submit" value="Cerrar Sesion"  name="salir">

</form>



<?php
  if (isset($_POST['salir'])){


    session_destroy();
     

    header("location:form.php");
  }

  mysqli_close($conn);
?>

</body>
</html>